// // The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:

const inventory = require('../data.js');
const findLastCarDetails = require('../problem2.js');
try {
    const lastCarDetails = findLastCarDetails(inventory);
    console.log(`Last car is a ${lastCarDetails.car_make} ${lastCarDetails.car_model}`);
} catch (error) {
    console.error(error.message);
}