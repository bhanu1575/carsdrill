// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory = require('../data.js');
const findNoOfOlderCars = require('../problem5.js');
let yearGivenByUser = 2000;
try {
    let {yearsOfOlderCars, countOfOlderCars} = findNoOfOlderCars(inventory,yearGivenByUser);
    console.log(yearsOfOlderCars);
    console.log(countOfOlderCars);
} catch (error) {
    console.error(error.message);
}