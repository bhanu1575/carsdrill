// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
const inventory = require('../data.js');
const findCarsByBrand = require('../problem6.js');

let brand1 = "BMW";
let brand2 = "Audi";

try {
    const cars = findCarsByBrand(inventory,brand1,brand2)
    for(car of cars) {
        console.log(JSON.stringify(car));
    }
} catch (error) {
    console.error(error.message);
}