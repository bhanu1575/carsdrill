// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const inventory = require('../data.js');

const getSortedCarModels = require('../problem3.js');


try {
    const sortedCarModels = getSortedCarModels(inventory);
    console.log(sortedCarModels);
} catch (error) {
    console.error(error.message);
}