// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:

const inventory = require('../data.js');
const findCarwithId = require('../problem1.js');
let id = 33; // enter id here.
try {
    const carDetails = findCarwithId(inventory, id );
    console.log(`Car ${carDetails.id} is a ${carDetails.car_year} ${carDetails.car_make} ${carDetails.car_model}`);
} catch (error) {
    console.error(error.message);
}
