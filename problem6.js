// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
const inventory = require('./data.js');

function findCarsByBrand(inventory,brand1,brand2) {
    const cars = [];
    if(Array.isArray(inventory) && typeof brand1 == 'string' && typeof brand2 == 'string' ) {
        for ( car of inventory ) {
            if(car.car_make == brand1 || car.car_make == brand2) {
                cars.push(car);
            }
        }
        if(cars.length == 0) {
            throw new Error('No Cars found');
        } else {
            return cars
        }
    } else {
        throw new Error("Arguments passed to the function is/are not valid datatypes")
    }
}

module.exports = findCarsByBrand;