// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const inventory = require('./data.js');

function getSortedCarModels(inventory) {
    if(Array.isArray(inventory)) {
        const carmodels = [];
        for (car of inventory) {
            if(typeof car == 'object' && car.hasOwnProperty('car_model')) {
                carmodels.push(car.car_model);
            }
        }
        carmodels.sort();
        return carmodels;
    } else {
        throw new Error('Argumets of a function sortCarModels is/are not valid type.');
    }
}

module.exports = getSortedCarModels;