// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory = require('./data.js');
const getYearsOfAllCars = require('./problem4.js');
function findNoOfOlderCars(inventory,yearGivenByUser) {
    if(Array.isArray(inventory) && typeof yearGivenByUser == 'number') {
        const yearsOfAllCars = getYearsOfAllCars(inventory);
        const yearsOfOlderCars = [];
        let countOfOlderCars = 0;
        for (year of yearsOfAllCars) {
            if(year<yearGivenByUser) {
                yearsOfOlderCars.push(year);
                countOfOlderCars++;
            }
        }
        return {yearsOfOlderCars, countOfOlderCars};
    } else {
        throw new Error("Argumets of function findNoOfOlderCars is/are not valid type");
    }
}

module.exports = findNoOfOlderCars;