// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:

let inventory = require('./data.js')

function findCarWithId( inventory, id ) {
    if(Array.isArray(inventory) && typeof id == 'number') {
        for (car of inventory) {
            if (car.id == id) {
                return car;
            }
        }
        throw new Error(`car with id: ${id} was not found`);
    } else {
        throw new Error('Argumets of a function findCarWithId is/are not valid type.');
    }
}

module.exports = findCarWithId