// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const inventory = require('./data.js');

function getYearsOfAllCars(inventory) {
    const years = [];
    if(Array.isArray(inventory)) {
        for ( car of inventory ) {
            if(typeof car == 'object' && car.hasOwnProperty('car_year')) {
                years.push(car.car_year);
            }
        }
        
        return years;
    } else {
        throw new Error ("Arguments of the function getYearsOfAllCars is not valid");
    }
}

module.exports = getYearsOfAllCars;